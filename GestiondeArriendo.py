
def autentificar():
    contador = 0
    maximo_intento = 3
    while contador < maximo_intento:
        usuario = input("Favor ingrese nombre de usuario")
        if len(usuario.strip()) > 0:
            if usuario.strip() == "1234":
                return [True, "Bienvenido al sistema, "]    # contador = 3
            else:
                print("Te equivocaste, vuelve a intentarlo")
            contador = contador +1
        else:
            print("es necesario una o mas letras")
    return [False , "Se han agotado los "+str(maximo_intento)+" intentos."]


def menu():
    opciones = ('a', 'b', 'c', 'd', 'f')   # tupla opciones del menu
    opciones_salida = ('s', 'si', 'n', 'no')
    seleccion = ''
    while len(seleccion.strip()) == 0:
        print("MENU PRINCIPAL")
        print("--------------")
        print("a .-Ingreso de los datos")
        print("b .-Calcular el valor de Luz")
        print("c .-Calcular el valor de Agua")
        print("d .-Calcular valor bruto de Arriendo")
        print("e .-Salir del programa")
        print("f .- Busqueda por nombre del arrendatario")
        seleccion = input("Favor coloque una opción")
        if seleccion == 'e':
            confirmar = ''
            while confirmar not in opciones_salida:
                confirmar = input("¿Esta seguro de querer salir? S = si, N = no")
                if confirmar.lower() == 's' or confirmar.lower() == 'si':
                    return seleccion.lower()    ## 'e'
                else:
                    seleccion = ''  
        else:
            if len(seleccion.strip()) > 0:      ## a - d
                if seleccion.lower() in opciones:
                    return seleccion.lower()
                else:
                    print("opcion no valida")
                    seleccion = ''

def ingresar_datos():
    nombre = ''
    while len(nombre.strip()) == 0:
        nombre = input("Ingrese nombre arrendatario")   # caracteres
        if len(nombre.strip()) == 0:
            print("Nombre no puede quedar en vacio")
    rut = ''
    while len(rut.strip()) == 0:
        rut = input("Ingrese rut arrendatario")
        if len(rut.strip()) == 0:
            print("Rut no puede quedar en vacio")
    valor = 0
    while valor == 0:
        valor = input("Ingrese pago del arriendo")
        if len(valor.strip()) == 0:
            print("Valor no puede quedar en vacio")
    return [nombre, rut, int(valor), int(valor)* 0.1, int(valor) * 0.15 ]



arriendos = {}    # diccionario [llave, valor]
contador = 1
resultado_autentificar = autentificar()
print( resultado_autentificar[1] )
if resultado_autentificar[0]:
    opcion = ''    ## inicialmente
    while opcion != 'e':
        opcion = menu()
        if opcion == 'a':
            arriendos[contador] = ingresar_datos()
            contador = contador + 1
            #print( arriendos )
        if opcion == 'b':
            for llave, valores in arriendos.items():
                print("Id : ", llave )
                print("Valor de Luz a pagar :", valores[3] )
                print("-------------------------------------")
                print()
                #print(llave, valores)
            #print("Valor de luz ", arriendos["luz"]) 
        if opcion == 'c':
            for llave, valores in arriendos.items():
                print("Id : ", llave )
                print("Valor de Agua a pagar :", valores[4] )
                print("-------------------------------------")
                print()
        if opcion == 'd':
            for llave, valores in arriendos.items():
                total = valores[2] - valores[3] - valores[4]
                print("Id : ", llave )
                print("Nombre del arrendatario :", valores[0] )
                print("Rut del arrendatario :", valores[1] )
                print("Valor arriendo bruto:", total )
                print("-------------------------------------")
                print()
        if opcion == 'e':
            pass
        if opcion == 'f':
            bandera = True
            nombre_busqueda = input("ingrese nombre del arrendatario a buscar")
            for llave, valores in arriendos.items():
                if nombre_busqueda == valores[0]:
                    total = valores[2] - valores[3] - valores[4]
                    print("Id : ", llave )
                    print("Nombre del arrendatario :", valores[0] )
                    print("Rut del arrendatario :", valores[1] )
                    print("Valor pagado :", valores[2] )
                    print("Valor de Luz a pagar :", valores[3] )
                    print("Valor de Agua a pagar :", valores[4] )
                    print("Valor arriendo bruto:", total )
                    print("-------------------------------------")
                    print()
                    bandera = False
            if bandera:
                print("El nombre del arrendatario no fue encontrado")
               
            
print("Programa terminado")

